/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_support_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/17 13:15:59 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/03 11:41:48 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** вспомогательные функции для обработки значений и строк
*/

/*
** освобождает память использованых в процессе работы списков
*/

void	ft_free_list(t_argdata *lst)
{
	if (lst != NULL)
	{
		if (lst->next != NULL)
			ft_free_list(lst->next);
		if (ft_strcmp(lst->form_input, "\0") != 0)
			ft_strdel(&(lst->form_input));
		free(lst);
		lst = NULL;
	}
}

/*
** меняет два значения в строке местами
*/

char	*ft_swap_sign(char *s, char t)
{
	if (((ft_strchr(s, t) - s) > 0))
	{
		if (t == 'x')
		{
			*(ft_strchr(s, 'x')) = '0';
			s[1] = 'x';
		}
		else
		{
			s[ft_strchr(s, t) - s] = '0';
			s[0] = t;
		}
	}
	return (s);
}

/*
** печать нулевых чаров
*/

void	ft_nullcharprint(char *s, t_argdata *lst, int *ans)
{
	if (lst->minus)
		ft_putchar('\0');
	s = ft_strnew(1);
	s = ft_apply_modifyers(s, lst);
	if (ft_strlen(s) != 0)
		s[ft_strlen(s) - 1] = '\0';
	ft_putstr(s);
	(*ans) += ft_strlen(s);
	if (!lst->minus)
		ft_putchar('\0');
	(*ans)++;
}
