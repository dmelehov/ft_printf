/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   curent_work.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/11 18:32:02 by dmelehov          #+#    #+#             */
/*   Updated: 2017/05/15 18:06:17 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** распечатка списка формата
*/

void			ft_printlist(t_argdata *data)
{
	printf("\n");
	printf("number of argument    ->%d<-\n", data->arg_num);
	printf("format string to parse |%s|\n", data->form_input);
	printf("hash ................ = %c\n", data->hash);
	printf("space ............... = %c\n", data->space);
	printf("zero ................ = %c\n", data->zero);
	printf("plus ................ = %c\n", data->plus);
	printf("minus ............... = %c\n", data->minus);
	printf("apostrophe .......... = %c\n", data->apostrophe);
	printf("star ................ = %c\n", data->star);
	printf("dollar .............. = %c\n", data->dollar);
	printf("type to convert ..... = %c\n", data->conversion);
	printf("lenght .............. = %c\n", data->lenght);
	printf("size ................ = %d\n", data->size);
	printf("precision ........... = %d\n", data->precision);
	printf("ans...... ........... = %d\n", data->ans);
}
