/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apply_lenght.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/17 13:15:59 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/03 11:41:48 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** применение модификаторов длинны к числовым типам данных
*/

/*
** применяет модификаторы длинны к di типам
*/

char	*ft_apply_lenght_di(intmax_t t, int base, t_argdata *lst)
{
	char *s;

	if (!(lst->lenght))
		s = ft_checkneg((int)t, base);
	else if (lst->lenght == 'l')
		s = ft_checkneg((long)t, base);
	else if (lst->lenght == 'L')
		s = ft_checkneg((long long)t, base);
	else if (lst->lenght == 'j')
		s = ft_checkneg((intmax_t)t, base);
	else if (lst->lenght == 'z')
		s = ft_checkneg((ssize_t)t, base);
	else if (lst->lenght == 'h')
		s = ft_checkneg((short)t, base);
	else if (lst->lenght == 'H')
		s = ft_checkneg((char)t, base);
	return (s);
}

/*
** применяет модификаторы длинны к uox типам
*/

char	*ft_apply_lenght_uox(t_argdata *lst, uintmax_t t, int base)
{
	char *s;

	if (!(lst->lenght))
		s = ft_uitoa((unsigned int)t, base);
	else if (lst->lenght == 'l')
		s = ft_uitoa((unsigned long)t, base);
	else if (lst->lenght == 'L')
		s = ft_uitoa((unsigned long long)t, base);
	else if (lst->lenght == 'j')
		s = ft_uitoa((uintmax_t)t, base);
	else if (lst->lenght == 'z')
		s = ft_uitoa((size_t)t, base);
	else if (lst->lenght == 'h')
		s = ft_uitoa((unsigned short)t, base);
	else if (lst->lenght == 'H')
		s = ft_uitoa((unsigned char)t, base);
	return (s);
}
