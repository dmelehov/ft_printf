/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_formatparser.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/17 13:15:59 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/03 11:41:48 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** проход по строке формата
** в зависимости от аргументов функции
** 1)заполняют структуру модификаторов
** 2)печатают строки не являющиеся модификаторами
** 3)пропускают первое или второе возвращая указатель на последующий символ
*/

/*
** поиск строки формата
*/

char		*ft_getformat(char *format, t_argdata *data)
{
	int i;

	i = 0;
	while (format[i] != '\0' && format[i] != '%')
	{
		if (ft_isascii(format[i]) && !(ft_strchr(RESERVED, format[i])))
			break ;
		if ((ft_isalpha(format[i]) == 1) && (ft_strchr(CONVERT, format[i])))
			break ;
		i++;
	}
	if (format[i] != '\0' && data != NULL)
	{
		data->form_input = ft_strsub(format, 0, i + 1);
		ft_fillstruct(data);
	}
	if (format[i] == '\0')
		return (format + i);
	return (format + i + 1);
}

/*
** создание элементов списка
*/

t_argdata	*ft_create_arglist(char *format, int argnum)
{
	static t_argdata	*data;

	if (argnum == 1)
		data = (t_argdata *)malloc(sizeof(t_argdata));
	else
	{
		data->next = (t_argdata *)malloc(sizeof(t_argdata));
		data = data->next;
	}
	*data = NEW_ARG;
	data->arg_num = argnum;
	ft_getformat(format, data);
	return (data);
}

/*
** пропуск или печать префиксов в строке формата
*/

char		*ft_printprefix(char *str, int *ans)
{
	while (!(str[0] == '%' && str[1] != '%') && *str != '\0')
	{
		while (*str != '%' && *str != '\0')
		{
			if (ans != NULL)
			{
				ft_putchar(str[0]);
				(*ans)++;
			}
			str++;
		}
		while (str[0] == '%' && str[1] == '%' && *str != '\0')
		{
			if (ans != NULL)
			{
				ft_putchar('%');
				(*ans)++;
			}
			str += 2;
		}
	}
	return (str);
}

/*
** парсинг строки формата
*/

t_argdata	*ft_formatparser(char *str, t_argdata *lst)
{
	int i;

	i = 1;
	while (*str != '\0')
	{
		str = ft_printprefix(str, NULL);
		if (str[0] == '%' && str[1] != '%')
		{
			if ((i == 1) && (str[1] != '\0'))
				lst = ft_create_arglist(str + 1, i);
			else if (str[1] != '\0')
				ft_create_arglist(str + 1, i);
			if (*str == '%')
				str = ft_getformat(str + 1, NULL);
			i++;
		}
	}
	return (lst);
}
