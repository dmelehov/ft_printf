/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 16:36:45 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/02 20:23:42 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft/libft.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <locale.h>
#include <limits.h>

int 	main(void)
{
	// setlocale(LC_NUMERIC, "da_DK");
	// char *s1;
	// int *d;
	// setlocale(LC_ALL, "");
	// char a;
	// char *s2;
	// s3 = "Dimas";
	// wchar_t t;
	// t = L'只';
	// s3 = "+-xXoO";
	// a = *(ft_strchr(s3, '+'));
	// int d = 2147483647;
	
	// long long intmax_t x1;
	// int d1 = 123;
	// int d2 = 1676767;
	// int p1 = 23;
	// unsigned long int p = 17;
	// float x = 0.0;
	// float x = 125.000007878;
	// float x = -1238979892.956729782738999;
	// float x = -1238979892.956729782738999;
	// long double x = 0.000089812598798;
	// long double x1 = x;
	

	// int i = 0;
	// // int j = 10;
	// // int z;
	// float x = 0.125;
	// double x1 = x;
	// if (x1 < 0)
	// {
	// 	x1 = -x1;
	// 	printf("-");
	// }

	// while (x1 > 2)
	// {
	// 	x1 /= 2;
	// 	i++;
	// }
	// while ((x1 * 2)  > 0 && (x1 * 2) < 2)
	// {
	// 	x1 *= 2;
	// 	i--;
	// }
	// printf("0x%jx.", (intmax_t)x1);
	// while (x1 > 0)
	// {
	// 	x1 = x1 - (intmax_t)x1;
	// 	x1  *= 16;
	// 	if (x1 != 0)
	// 		printf("%jx", (intmax_t)x1);
	// }
	// printf("p%+d\n", i);
	// printf("%a\n", x);

	// x1 = x1 * 10;
	// printf("[1)%Lf]\n", x1);
	// x1 -= (x1 - (int)x1);
	// printf("[2)%Lf]\n", x1);
	// x1 = x1 / 10;
	// printf("[3)%Lf]\n", x1);
	// printf("[4)%d]\n", (int)x1);
	
	// z = j;
	// // printf("[0)%Lf]\n", x1);
	// while (z-- > 0)
	// 	x1 = x1 * 16;
	// // printf("[1)%Lf]\n", x1);
	// if (x1 - (intmax_t)x1 > 0.5)
	// {
	// 	x1 = x1 + 1;
	// 	x1 = x1 - (x1 - (intmax_t)x1);
	// }
	// // printf("[2)%Lf]\n", x1);
	// while (z++ < j - 1)
	// 	x1 = x1 / 16;
	// printf("[3)%.15Lf]\n", x1);
	
	// while (j++ < 13)
	// 	x2 *= 10;

	
	// printf("0x%d.%jxp%+d\n", (int)x1, (intmax_t)x2, i);
	
	// long double x = 9.88123561837225;
	// long double x = 0.000988123561837225;
	// long double x = 0.0;

	// s1 = "Alice";

	// float x = 0.0 / 0.0;
	// float x = -0.12348901234789012345678901234567890123456789012345678901234567890 / 0.000000000000000000000000000000000000000000000001;
	// union	{
	// 	float fl;
	// 	uint32_t dw;
	// } u_f;
	// u_f.fl = x;
	// int s = ( u_f.dw >> 31 ) ? -1 : 1;
	// int e = ( u_f.dw >> 23 ) & 0xFF;
	// int m = ( u_f.dw & 0x7FFFFF ) << 1;
	// // printf("{%zu}\n", sizeof(d));

	// printf("%d\n%d\n%x\n", s, (e - 127), m);
	// printf("[%a]\n", x);
	// printf("%x\n", 234500);
	// wchar_t t;
	// t = L'我';
	// s2 = L"我是一只猫。";
	// s3 = 'A';
	int i = 0;
	// int j = 2;
	// int z = 3;

	// double x = -2147483648.0;
	// double x = -8000;
	// printf("%2$6d\n%3$a\n%1$d\n", i, j, x);
	// *j = 1;
	// float x = 2.0;
	// float x = 90.8988178;
	// float x = 42.42;
	float x = 503.0;
	// float x = 23767.6773492798725;
	// double x = 9999.729387799489023094809289;
	// x = x - (intmax_t)x;
	// printf("{%d}\n",    printf("{%.54f}", x));
	// printf("{%jd,", (intmax_t)x);
	// while (x != 0)
	// {
	// 	x *= 10;
	// 	printf("%jd", (intmax_t)x);
	// 	x = x - (intmax_t)x;
	// 	i++;
	// }
	// printf("}\n");
	printf("{%d}\n", ft_printf("{%f}", x));
	printf("{%d}\n",    printf("{%f}", x));
	
	printf("\n-----What the Fuck is going on here -----\n\n");
	
	// while (i <= 7)
	// {
	// 	printf("PRECISION == %d\n", i);
	// 	// printf("{%d}\n", ft_printf("{%.*e|%.*f|%.*g}", i, x, i, x, i, x));
	// 	// printf("{%d}\n",    printf("{%.*e|%.*f|%.*g}", i, x, i, x, i, x));
	// 	printf("{%d}\n", ft_printf("{%.*a}", i, x));
	// 	printf("{%d}\n",    printf("{%.*a}", i, x));
	// 	i++;
	// }
	// printf("{%d}\n", ft_printf("{%f}", x));
	// printf("{%d}\n",    printf("{%f}", x));
	// printf("{%d}\n", ft_printf("{%", x, x
	// printf("{%d}\n",    printf("{%.*La}", i, x));

	// printf("{%d}\n", ft_printf("{%F\t%'f}", x, x));
	// printf("{%d}\n",    printf("{%F\t%'f}", x, x));
	// // printf("\n");
	// printf("{%d}\n", ft_printf("{%d\t%'d}", i, i));
	// printf("{%d}\n",    printf("{%d\t%'d}", i, i));
	// printf("\n");
	// printf("{%d}\n", ft_printf("{%.3a}", x));
	// printf("{%d}\n",    printf("{%.3a}", x));
	// printf("%c\n", 1["Dimas"]);
	// printf("%zu\n", sizeof(x1));
	
	// if (-1 == EOF)
	// 	printf("SRAV\n");
	// printf("%c\n", (unsigned char)EOF + 48);
	// printf("{%d}\n",    printf("-->[%f]<--", x));
	// printf("{%d}\n", ft_printf("{%*3d}", 5, 0));
	// printf("{%d}\n",    printf("{%*3d}", 5, 0));
	// printf("\n");
	// printf("{%d}\n", ft_printf("{%lc}", t));
	// printf("{%d}\n",    printf("{%lc}", t));
	// printf("\n");
	// printf("{%d}\n", ft_printf("%2.9p", 1234));
	// printf("{%d}\n",    printf("%2.9p", 1234));
	// printf("%s\n", 2 + s3);
	// printf(&1["\021%six\012\0"], 1["have"] + "fun" - 0x60);

	return (0);
}
