/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brainfuck.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 16:36:45 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/02 20:23:42 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

char	*ft_brain(char *str)
{
	int i;

	i = 1;
	while (i && str++)
	{
		if (*str == '[')
			i++;
		if (*str == ']')
			i--;
	}
	return (str);
}

char	*ft_fuck(char *str)
{
	int i;

	i = 1;
	while(i && str--)
	{
		if (*str == '[')
			i--;
		if (*str == ']')
			i++;
	}
	return (str);
}

void	ft_brainfuck(char *str, char *s)
{
	while(*str != '\0')
	{
		if (*str == '>')
			s++;
		if (*str == '<')
			s--;
		if (*str == '+')
			(*s)++;
		if (*str == '-')
			(*s)--;
		if (*str == '.')
			write(1, &s[0], 1);
		if (*str == '[' && *s == 0)
			str = ft_brain(str);
		if (*str == ']' && *s != 0)
			str = ft_fuck(str);
		str++;
	}
}

int		main(int argc, char **argv)
{
	char 	*s;
	int		i;

	i = 0;
	s = (char *)malloc(sizeof(char) * 2048);
	s[2048] = '\0';
	while (s[i] != '\0')
		s[i] = 0;
	if (argc == 2)
		ft_brainfuck(argv[1], s);
	else
		write(1, "\n", 1);
	return (0);
}
