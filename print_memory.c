/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_memory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 16:36:45 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/02 20:23:42 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <stdio.h>
# include <unistd.h>


int		ft_print_hexdec(int t)
{
	char const	*base = "0123456789abcdef";
	int i;

	i = 1;	
	if (t >= 16)
		i += ft_print_hexdec(t / 16);
	write(1, &base[t % 16], 1);
	return (i);
}


void	print_memory(const void *addr, size_t size)
{
	int	*t;
	int i;
	int j;
	int sz;

	sz = size / 4;
	i = 0;
	j = 0;
	t = (int *)addr;
	while (i < sz)
	{
		if (t[i] < 16)
		{
			write(1, "0", 1);
			j++;
		}
		j += ft_print_hexdec(t[i]);
		while (j < 8)
		{
			write(1, "0", 1);
			j++;
			if (j % 4 == 0)
				write(1, " ", 1);
		}
		i++;
		if (i == sz && i % 4 != 0)
		{
			while (i % 4 != 0)
			{
				write(1, "          ", 10);
				i++;
			}
		}
		if (i % 4 == 0)
		{
			j = i - 4;
			while (j < i && j < sz)
			{
				if (t[j] > 31 && t[j] < 127)
				{
					write(1, &t[j], 1);
					write(1, "...", 3);
				}
				else
					write(1, "....", 4);
				j++;
			}
			write(1, "\n", 1);
		}
		j = 0;
	}
}

int	main(void)
{
	int	tab[10] = {0, 23, 150, 255,
	              12, 16,  21, 42};

	print_memory(tab, sizeof(tab));
	return (0);
}

// ................
// ............*...
// ........
