/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_memory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmelehov <dmelehov@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/02 16:36:45 by dmelehov          #+#    #+#             */
/*   Updated: 2017/04/02 20:23:42 by dmelehov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>

#define BASE "0123456789abcdef"
#define BLOCK 16

void	ft_print_chars(unsigned char *t, int mem_block)
{
	int i;

	i = 0;
	while (i < mem_block)
	{
		if (t[i] > 31 && t[i] < 127)
			write(1, &t[i], 1);
		else
			write(1, ".", 1);
		i++;
	}
}

void	ft_print_hexdec(unsigned char *t, int mem_block)
{
	int i;

	i = 0;
	while (i < mem_block)
	{
		write(1, &BASE[t[i] / 16], 1);
		write(1, &BASE[t[i] % 16], 1);
		i++;
		if (!(i % 2))
			write(1, " ", 1);
	}
	if (mem_block < 16)
		while (i++ < 16)
		{
			write(1, "  ", 2);
			if (!(i % 2))
				write(1, " ", 1);
		}
}

void	print_memory(const void *addr, size_t size)
{
	unsigned char	*t;
	size_t			i;

	t = (unsigned char *)addr;
	i = 0;
	while (i < size)
	{
		if (size - i > 16)
		{
			ft_print_hexdec(t, 16);
			ft_print_chars(t, 16);
		}
		else
		{
			ft_print_hexdec(t, size - i);
			ft_print_chars(t, size - i);
		}
		write(1, "\n", 1);
		t = t + BLOCK;
		i = i + BLOCK;
	}
}

int		main(void)
{
	int	tab[12] = {70, 85, 67, 75, 89, 79, 85, 199, 17, 25};

	print_memory(tab, sizeof(tab));
	return (0);
}

